<?php
namespace Kabaz\KzCeTemplating\Hooks\PageLayoutView;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Service\FlexFormService;
use \TYPO3\CMS\Backend\View\PageLayoutViewDrawItemHookInterface;
use \TYPO3\CMS\Backend\View\PageLayoutView;

/**
 * Contains a preview rendering for the page module of CType="yourextensionkey_newcontentelement"
 */
class ColPosContentPreviewRenderer implements PageLayoutViewDrawItemHookInterface
{

    
   /**
    * Preprocesses the preview rendering of a content element of type "My new content element"
    *
    * @param \TYPO3\CMS\Backend\View\PageLayoutView $parentObject Calling parent object
    * @param bool $drawItem Whether to draw the item using the default functionality
    * @param string $headerContent Header content
    * @param string $itemContent Item content
    * @param array $row Record row of tt_content
    *
    * @return void
    */
    public function preProcess(PageLayoutView &$parentObject, &$drawItem, &$headerContent, &$itemContent, array &$row) {
        if ($row['CType'] === 'kzcetemplating_colposcontent') {
            
            $flexform = $this->cleanUpArray(GeneralUtility::xml2array($row['pi_flexform']), array('data', 'sDEF', 'lDEF', 'vDEF'));

            $content = '<p><b>ColPos Content</b><br />';
            $fields = [];
            if(isset($flexform['main']['settings.column']))
                $fields[] = 'Column: '.$flexform['main']['settings.column'];
            
            if(isset($flexform['main']['settings.pageuid']))
                $fields[] = 'PageId: '.$flexform['main']['settings.pageuid'];
            
            $content .= implode("<br />",$fields) . '</p>';
            $itemContent .= $parentObject->linkEditContent($content, $row);
            
            $drawItem = false;
        }
    }

    public function cleanUpArray(array $cleanUpArray, array $notAllowed) {
        $cleanArray = array();
        foreach ($cleanUpArray as $key => $value) {
            if (in_array($key, $notAllowed)) {
                return is_array($value) ? $this->cleanUpArray($value, $notAllowed) : $value;
            } else {
                if (is_array($value)) {
                    $cleanArray[$key] = $this->cleanUpArray($value, $notAllowed);
                }
            }
        }
        return $cleanArray;
    }
    
}