<?php 

namespace Kabaz\KzCeTemplating\Hooks;


class SetCObjectDepthCounterHook {    

  protected $confArray;
  protected $defaultCObjectDepthCounter = 100;
  
	/**
	 * Initializes the class.
	 */
	public function __construct() {
	}


	protected function setCObjectDepthCounter() {
		$this->readConf();
		
		$GLOBALS['TSFE']->cObjectDepthCounter = $this->confArray["cObjectDepthCounter"];
	}
	

	public function contentPostProc_output(&$params) { $this->setCObjectDepthCounter(); } // Too late
	public function tslib_fe_PostProc(&$params) { $this->setCObjectDepthCounter(); } // Deprecated
	public function tslib_fe_connectToDB(&$params) { $this->setCObjectDepthCounter(); } // Deprecated
	
	
	protected function readConf() 
	{
        if (!$this->confArray) {
            $this->confArray = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['kz_ce_templating']);
			
            if (empty($this->confArray['cObjectDepthCounter'])) {
                $this->confArray['cObjectDepthCounter'] = $this->defaultCObjectDepthCounter;
            }
        }	
	}
}

?>
