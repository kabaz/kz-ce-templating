<?php

return [
    'frontend' => [
        'kabaz/kzcetemplating/tsfe' => [
            'target' => \Kabaz\KzCeTemplating\Middleware\TsfeMiddleware::class,
            'before' => [
            ],
            'after' => [
                'typo3/cms-frontend/prepare-tsfe-rendering',
            ],
        ]
    ],
	// NO TSFE IN BACKEND -> Workaroung TODO
/*    'backend' => [
        'kabaz/kzcetemplating/tsfe' => [
            'target' => \Kabaz\KzCeTemplating\Middleware\TsfeMiddleware::class,
            'before' => [
            ],
            'after' => [
                'typo3/cms-backend/authentication',
            ],
        ]
    ]*/
];