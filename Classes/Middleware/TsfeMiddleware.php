<?php
declare(strict_types = 1);
namespace Kabaz\KzCeTemplating\Middleware;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;


class TsfeMiddleware implements MiddlewareInterface
{
	
  protected $confArray;
  protected $defaultCObjectDepthCounter = 100;
  
	/**
	 * Sets the cObjectDepthCounter
	 * attribute to $request object
	 *
	 * @param ServerRequestInterface $request
	 * @param RequestHandlerInterface $handler
	 * @return ResponseInterface
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		$this->readConf();
		
		/*
		$request = $request->withAttribute(
			'normalizedParams',
			new NormalizedParams(
				$request,
				$GLOBALS['TYPO3_CONF_VARS'],
				Environment::getCurrentScript(),
				Environment::getPublicPath()
			)
			
		);*/
		
		$GLOBALS['TSFE']->cObjectDepthCounter = $this->confArray["cObjectDepthCounter"];
		
        // Continue the regular stack if no maintenance mode is active
        return $handler->handle($request);
	}
	
	
	protected function readConf() 
	{
        if (!$this->confArray) {
            $this->confArray = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['kz_ce_templating']);
			
            if (empty($this->confArray['cObjectDepthCounter'])) {
                $this->confArray['cObjectDepthCounter'] = $this->defaultCObjectDepthCounter;
            }
        }	
	}
}

