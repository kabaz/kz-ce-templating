<?php

// Adds the content element to the "Type" dropdown
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
   array(
      'LLL:EXT:kz_ce_templating/Resources/Private/Language/locallang.xlf:colposcontent',
      'kzcetemplating_colposcontent',
      'EXT:kz_ce_templating/Resources/Public/Icons/content-special-shortcut.svg'
   ),
   'CType',
   'kz_ce_templating'
);



// Configure the default backend fields for the content element
$GLOBALS['TCA']['tt_content']['types']['kzcetemplating_colposcontent'] = [
	'showitem' => '
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.general;general,
         --palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:palette.header;header,
		 pi_flexform,
		--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language,
			--palette--;;language,
      --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xml:tabs.access,
		--palette--;;hidden,
		--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
	',
];


\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    '*',
    'FILE:EXT:kz_ce_templating/Configuration/FlexForms/ContentElements/ColPosContent.xml',
    'kzcetemplating_colposcontent'
);



?>