<?php
if (!defined ('TYPO3_MODE')) die ('Access denied.');


(function($_EXTKEY) {
          
    // Register for hook to show preview of tt_content element of CType="yourextensionkey_newcontentelement" in page module
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem']['kzcetemplating_colposcontent'] =
       \Kabaz\KzCeTemplating\Hooks\PageLayoutView\ColPosContentPreviewRenderer::class;
       
        
})('kz_ce_templating');

