
# Backend Layouts
mod.web_layout.BackendLayouts {
	
	1 {
		title = Template Layout
		config {
			backend_layout {
				colCount = 1
				rowCount = 1
				rows {
					1 {
						columns {
							1 {
								name = Template Content
								colPos = 0
							}
						}
					}
				}
			}
		}
	}
	
}

mod.wizards.newContentElement.wizardItems.special {
   elements {
      kzcetemplating_colposcontent {
         iconIdentifier = content-special-shortcut
         title = LLL:EXT:kz_ce_templating/Resources/Private/Language/locallang.xlf:colposcontent.wizard.title
         description = LLL:EXT:kz_ce_templating/Resources/Private/Language/locallang.xlf:colposcontent.wizard.description
         tt_content_defValues {
            CType = kzcetemplating_colposcontent
         }
      }
   }
   show := addToList(kzcetemplating_colposcontent)
}