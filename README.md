# Typo3 Content-Element Templating Engine

## What does it do

It gives you the opportunity to build up complete Website-Layouts without touching any HTML File nor using any Fluid-Template (except the base empty template from this extension).

In combination with a extension like "flux" or "gridelements" you can build up everything with Content-Elements.


## What does it do in detail

The extension has two main components. It searches for pages in a folder. These pages are the frontend layouts. They can be designed as anything they want. 
In every page should be one Backend-Layout record. This is the indicator for the Backend-Layout select in your normal pages and also the layout in the backend.
In normal they should have the same structure, like "Header", "Menu", "Content", "Sidebar", "Footer". You should note your Column-Numbers.

The second part is the dynamic way the extension loads site content from your pages. Therefore it has a special content element in "Special" named: "ColPos Content".
You should use this CE in a layout page. It only needs the number of the Column-Number what you used in you Backend-Layout record. 
If you have e.g. 5 columns in your Backend-Layout record you normally should also have 5 ColPos CEs in your Layout-Page.

When you select the Backend-Layout in your page, it will use the created backend layout record for you backend, and directly the frontend layout page as frontend output.


## Quick Start

1. Install the extension
2. Include "kz_ce_templating" template in your TypoScript Template (needed in page TS and Layouts-Folder TS!!)
3. Add a Backend-Folder in Typo3: The Layouts-Folder
4. Set the ID of the Layouts-Folder in TypoScript (see Configuration > TypoScript Constants)
5. Create a standard page with Backend-Layout: "Template". Name it e.g. "Page Layout 1"
6. Create a Backend-Layout Record in this page with your desired layout columns and name it also "Page Layout 1"
7. Create your Frontend-Layout with CEs. Use the "Special" > "ColPos Content" to display a "Column Content" later in your page
8. Create a new page with Backend-Layout: "Page Layout 1"


## Configuration

### TypoScript Constants

Backend Page-Folder, where Layout-Pages should be put:
```
plugin.tx_kzcetemplating.settings.layoutsPID =
```

## Example

Site Tree Example
![Site Tree Example](/Readme/site-tree-example.jpg?raw=true)

Basis Template Include
![Basis Template Include](/Readme/include-basis-template.jpg?raw=true)

Layout-Page Backend-Layout Select
![Layout-Page Backend-Layout Select](/Readme/layout-page-backend-layout.jpg?raw=true)

Layout-Page Backend-Layout Record
![Layout-Page Backend-Layout Select](/Readme/layout-page-backend-layout-record.jpg?raw=true)

Layout-Page Frontend Layout
![Layout-Page Frontend Layout](/Readme/layout-page-frontend.jpg?raw=true)

Page Backend-Layout Select
![Layout-Page Backend-Layout Select](/Readme/page-backend-layout.jpg?raw=true)

Page Backend
![Page Backend](/Readme/page-backend.jpg?raw=true)
